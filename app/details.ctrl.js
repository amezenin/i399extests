(function () {
    'use strict';

    angular.module('app').controller('DetailsCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var vm = this;

        vm.task = {};
        vm.back = back;

        init();

        function init() {
            $http.get('api/tasks/' + $routeParams.id).then(function (result) {
                vm.task = result.data;
            });
        }

        function back() {
            $location.path('/list');
        }

    }

})();

